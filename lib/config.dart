import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:logging/logging.dart';

final log = Logger('DacitLogger');
String backendUrl = 'http://localhost:8000';
FlutterSecureStorage storage = const FlutterSecureStorage();
String? djangoToken;
