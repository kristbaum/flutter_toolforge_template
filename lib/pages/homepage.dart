import 'package:flutter/material.dart';
import 'package:flutter_toolforge_template/config.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  Future<void> fetchUser() async {
    log.info("Testing if user is logged in");
    if (djangoToken == null) {
      throw Exception('No token found');
    }
    log.info("Token found, testing if token is valid");
    final response = await http.get(Uri.parse('$backendUrl/auth/user/'));
    if (response.statusCode == 200) {
      log.info("Token is valid");
    } else {
      log.info("Token is invalid");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter OAuth Demo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FutureBuilder<void>(
              future: fetchUser(),
              builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicator();
                } else if (snapshot.hasError) {
                  log.info(snapshot.error);
                  return ElevatedButton(
                    onPressed: () {
                      launchUrl(
                        Uri.parse(
                          'http://localhost:8000/auth/mediawiki/url/',
                        ),
                        webOnlyWindowName: '_self',
                      );
                    },
                    child: const Text('Login with Wikimedia'),
                  );
                } else {
                  return Text('Logged in as');
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
