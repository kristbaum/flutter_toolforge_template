import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_toolforge_template/config.dart';
import 'package:flutter_toolforge_template/pages/homepage.dart';
import 'package:logging/logging.dart';

void main() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    // ignore: avoid_print
    print('${record.level.name}: ${record.time}: ${record.message}');
  });
  // Initialize secure storage
  log.info("Initializing secure storage");
  storage.readAll().then((value) {
    log.info(value);
    djangoToken = value['token'];
  });
  if (kDebugMode) {
    backendUrl = 'http://localhost:8000';
  } else {
    backendUrl = 'https://sometool.toolforge.org';
  }
  log.info("Backend URL: $backendUrl");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Toolforge',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }
}
