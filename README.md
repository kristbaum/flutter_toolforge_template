# Flutter Toolforge Template

Template for toolforge.org services using flutter, including OAUTH

# Todo

Fix OAUTH

# Setup

Run `flutter web build` locally and commit the build folder to your project (watch out for included secrets!)

Clone the project via https in toolforge in your $HOME folder.

Symlink to the build/web folder:
```
ln -s $HOME/flutter_toolforge_template/build/web $HOME/public_html
```

Create a service.template file with the following contents in $HOME (we are using the default backend, because we only need the webserver):

```
# Toolforge webservice template
# Provide default arguments for `webservice start` commands for this tool.
#
# Uncomment lines below and adjust as needed

# Set backend cluster to run this webservice (--backend={gridengine,kubernetes})
backend: kubernetes

# Set Kubernetes cpu limit (--cpu=...)
#cpu: 500m

# Set Kubernetes memory limit (--mem=...)
#mem: 512Mi

# Set ReplicaSet size for a Kubernetes deployment (--replicas=...)
#replicas: 2

# Runtime type
# See "Supported webservice types" in `webservice --help` output for valid values.
# type: 

# Extra arguments to be parsed by the chosen TYPE
#extra_args:
#  - arg0
#  - arg1
#  - arg2
```


# Development setup for Ubuntu

```
sudo snap install --classic flutter
sudo snap install --classic android-studio
sudo snap install chromium
echo -e "export CHROME_EXECUTABLE=/snap/bin/chromium" >> ~/.bashrc
echo -e "alias google-chrome='chromium'" >> ~/.bashrc
```